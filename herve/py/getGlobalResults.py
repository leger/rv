#!/usr/bin/python

import cgi
import cgitb
import sys

import psycopg2

import json

#cgitb.enable(display=0, logdir="/tmp/hervelog/")
cgitb.enable()

from config import *

form = cgi.FieldStorage()

try:
    jid=int(form['jid'].value)
except (KeyError, ValueError):
    sys.exit(1)


db = psycopg2.connect(dbstr)
cur = db.cursor()

cur.execute("SELECT cum_elev,energy,dist,mean_speed,time FROM rv_jobs WHERE jid=%s;",(jid,))

if cur.rowcount<1:
    sys.exit(1)

ligne=cur.fetchone()

cum_elev=ligne[0]
energy=ligne[1]
dist=ligne[2]
mean_speed=ligne[3]
time=ligne[4]

cum_elev=int(round(cum_elev))
energy=int(round(energy/1000))
dist=int(round(dist))*1./1000
mean_speed=round(mean_speed*36)/10

tmin=time/60
time=("%dh%02d" % (int(tmin/60),int(tmin%60)))

print "Content-type: application/json\n"
print json.dumps({'cum_elev':cum_elev,'energy':energy,'dist':dist,'mean_speed':mean_speed,'time':time})

sys.stdout.flush()



