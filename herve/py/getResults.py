#!/usr/bin/python

import cgi
import cgitb
import sys

import psycopg2

import json




#cgitb.enable(display=0, logdir="/tmp/hervelog/")
cgitb.enable()

import rv

form = cgi.FieldStorage()

try:
    jid=int(form['jid'].value)
except (KeyError, ValueError):
    sys.exit(1)

points = rv.getPoints(jid)

bounds = [
        [min(map(lambda x:x.lat,points)), min(map(lambda x:x.lon,points))],
        [max(map(lambda x:x.lat,points)), max(map(lambda x:x.lon,points))]
         ]

allres = [
          rv.getFeatColl(points,3200,simplify=True),
          rv.getFeatColl(points,1600,simplify=True),
          rv.getFeatColl(points,800,simplify=True),
          rv.getFeatColl(points,400),
          rv.getFeatColl(points,200),
          rv.getFeatColl(points,100),
          rv.getFeatColl(points,50),
          rv.getFeatColl(points,25),
         ];

print "Content-type: application/json\n"
print json.dumps({'allres':allres,'bounds':bounds});

sys.stdout.flush()



