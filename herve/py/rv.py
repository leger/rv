#!/usr/bin/python

import psycopg2
import sys
import numpy as np
import colorsys
import gpxpy
import gpxpy.gpx

from config import *

class Node:
    """node with routing information

    Node infos :
        id: node id in the OSM database
        lon: longitude
        lat: latitude
        height: height obtained by bi-linear interpolation from SRTM data

    Routing infos :
        speed: speed in km/h at this node
        dist: travelled distance in meters from the beginning
        cum_elev: cumulated elevation in meter from the begining
        energy: supplied energy in Joules from the begining
        time: elpased time in seconds from the begining

    """

    def __init__(self, lon, lat, height, speed, dist, cum_elev, energy, time):
        self.lon, self.lat, self.height = lon, lat, height
        self.speed, self.dist, self.cum_elev = speed, dist, cum_elev
        self.energy, self.time = energy, time

    def __repr__(self):
        return "\n" +\
               "node(\n" +\
               "     lon = %f,\n" % self.lon +\
               "     lat = %f,\n" % self.lat +\
               "     height = %f,\n" % self.height +\
               "     speed = %f,\n" % self.speed +\
               "     dist = %f,\n" % self.dist +\
               "     cum_elev = %f,\n" % self.cum_elev +\
               "     energy = %f,\n" % self.energy +\
               "     time = %f\n" % self.time +\
               "    )\n"

def fcolor(pente):
    repere=.05
    if(pente > -repere and pente<repere):
        col=colorsys.hsv_to_rgb((1.-pente/repere)/6,1,.8)
    elif(pente>=repere and pente<2*repere):
        col=colorsys.hsv_to_rgb(0,1,.8*(2-pente/repere))
    elif(pente>=2*repere):
        col=colorsys.hsv_to_rgb(0,1,0)
    else:
        col=colorsys.hsv_to_rgb(1./3,1,.8)

    return "#%02x%02x%02x" % tuple(map(lambda x:int(255*x+.5),col))

def getParameters(jid):
    db = psycopg2.connect(dbstr)
    cur = db.cursor()

    cur.execute("SELECT mass,speedref,scx,cr,winddir,windspeed,criterion FROM rv_jobs WHERE jid=%s;",(jid,))

    if cur.rowcount<1:
        return None

    ligne=cur.fetchone()
    return {
            'mass':ligne[0],
            'speedref':ligne[1],
            'SCx':ligne[2],
            'Cr':ligne[3],
            'winddir':ligne[4],
            'windspeed':ligne[5],
            'criterion':ligne[6]
           }

def getMarkers(jid):
    db = psycopg2.connect(dbstr)
    cur = db.cursor()

    cur.execute("SELECT ST_X(geom),ST_Y(geom) FROM rv_waypoints WHERE jid=%s ORDER BY rank;",(jid,))

    if cur.rowcount<1:
        return None

    markers = []

    for ligne in cur:
        markers.append([ligne[1],ligne[0]])

    return markers

def getPoints(jid):

    db = psycopg2.connect(dbstr)
    cur = db.cursor()

    cur.execute("SELECT ST_X(geom),ST_Y(geom),height,speed,dist,cum_elev,energy,time FROM rv_results WHERE jid=%s ORDER BY rank;",(jid,))

    if cur.rowcount<1:
        return None

    lPoints=[]
    for ligne in cur:
        n=Node(
                lon=ligne[0],
                lat=ligne[1],
                height=ligne[2],
                speed=ligne[3],
                dist=ligne[4],
                cum_elev=ligne[5],
                energy=ligne[6],
                time=ligne[7]
              )
        lPoints.append(n)

    cur.close()
    db.close()

    return lPoints

def getGPX(jid,minimal=True,step=0,nptsmax=0):
    
    db = psycopg2.connect(dbstr)
    cur = db.cursor()

    cur.execute("SELECT ST_X(geom),ST_Y(geom),height,way_id,dist FROM rv_results WHERE jid=%s ORDER BY rank;",(jid,))
    
    if cur.rowcount<1:
        return None

    Lpts = []
    for ligne in cur:
        lon=ligne[0]
        lat=ligne[1]
        height=ligne[2]
        way_id=ligne[3]
        dist=ligne[4]

        Lpts.append( [lon,lat,height,way_id,dist] )
    
    cur.close()
    db.close()

    total_dist = Lpts[-1][4]

    if nptsmax>0:
        if total_dist>step*nptsmax:
            step = total_dist*1.0/nptsmax

    for i in range(1,len(Lpts)):
        if Lpts[i-1][3] == 0:
            Lpts[i-1][3]=Lpts[i][3]

    gpx_route = gpxpy.gpx.GPXRoute()
    
    current_way_id=0
    current_dist=0

    for i in range(len(Lpts)):
        we_use_it = False
        if minimal:
            if Lpts[i][3]!=current_way_id:
                we_use_it=True
        else:
            if Lpts[i][4]-current_dist > step:
                we_use_it=True

        if i==0 or i==len(Lpts)-1:
            we_use_it=True

        if we_use_it:
            current_dist = Lpts[i][4]
            current_way_id=Lpts[i][3]
            gpx_route.points.append(
                gpxpy.gpx.GPXRoutePoint(
                    longitude=Lpts[i][0], 
                    latitude=Lpts[i][1],
                    elevation=Lpts[i][2]
                )
            )
            
    gpx = gpxpy.gpx.GPX()
    gpx.name='RV result %d' % jid
    gpx_route.name='RV result %d' % jid
    gpx.author='RV routing engine'
    gpx.creator='RV routing engine'
    gpx.url= url_jid % jid

    gpx.routes.append(gpx_route)

    return gpx.to_xml()

def getFeatColl(lPoints_ref,pas,simplify=False):
    lPoints = lPoints_ref[:]

    llPoints=[]
    lastpts=lPoints.pop(0)
    enCours=[lastpts]

    i=0

    while True:
        # on construit de i*pas a (i+1)*pas
        # on ajoute les points du segment
        while lPoints[0].dist<(i+1)*pas:
            lastpts=lPoints.pop(0)
            enCours.append(lastpts)
            if(len(lPoints)==0):
                llPoints.append(enCours)
                break

        if(len(lPoints)==0):
            break

        # on cree un pts
        pts=lPoints[0]
        r=((i+1)*pas-lastpts.dist)/(pts.dist-lastpts.dist)
        s=1-r

        # npts=r*pts+s*lastpts
        npts = Node(
                    lon = r*pts.lon+s*lastpts.lon,
                    lat = r*pts.lat+s*lastpts.lat,
                    height = r*pts.height+s*lastpts.height,
                    speed = np.sqrt(r*np.power(pts.speed,2)+s*np.power(lastpts.speed,2)),
                    dist = r*pts.dist+s*lastpts.dist,
                    cum_elev = r*pts.cum_elev + s*lastpts.cum_elev,
                    energy = r*pts.energy + s*lastpts.energy,
                    time = r*pts.time + s*lastpts.time
                   )

        enCours.append(npts)
        llPoints.append(enCours)
        enCours=[npts]
        i+=1


    kmax=int(round(4.*(65./pas)*(65./pas)+1))
    a=np.array([1.])
    a0=np.array([0.])
    for k in range(1,kmax):
        a=(np.concatenate((a,a0))+np.concatenate((a0,a)))/2

    fpente = lambda a:(a[-1].height-a[0].height)/(a[-1].dist-a[0].dist)
    pentes=np.convolve(map(fpente,llPoints),a,'same')

    lfeatures=[]

    for k in range(len(llPoints)):
        seg=llPoints[k]
        if(simplify):
            seg=[seg[0],seg[-1]]
        ls = {'type':'LineString', 'coordinates': map(lambda pts: [pts.lon,pts.lat],seg)}
        col = fcolor(pentes[k])
        d = round((seg[0].dist+seg[-1].dist)/2/100)/10
        h = int(round((seg[0].height+seg[-1].height)/2))
        v = round((seg[0].speed+seg[-1].speed)/2*36)/10
        ce = int(round((seg[0].cum_elev+seg[-1].cum_elev)/2))
        t = round((seg[0].time+seg[-1].time)/2/60)
        t = "%dh%02d" % (int(t/60),(t%60))

        properties = {
                        'color': col,
                        'dist': d,
                        'height': h,
                        'speed': v,
                        'cum_elev': ce,
                        'time': t,
                        'pente': round(pentes[k]*1000)/10
                     }
        feat = {
                'type': 'Feature',
                'geometry': ls,
                'properties': properties
               }


        lfeatures.append(feat)

    featColl = {
                'type': 'FeatureCollection',
                'features': lfeatures
               }

    return featColl

