#include<cmath>

void model::calc_model(double & d_V1, double & d_Ea, double & d_Ec, double d_V0, double d_vent, double d_d, double d_Ep, double d_Er) const
{
    double a,b,c,d,p,q,discri;


    a=(d_d * d_rho_air * d_SCx + 2.0 * d_masse) / (8.0 * d_d);


    b=((d_d * d_rho_air * d_SCx + 2.0 * d_masse) * d_V0 + 4.0 * d_d * d_rho_air * d_vent * d_SCx) / (8.0 * d_d);

    double d_prov_c_0 = std::pow(d_V0,2.0); // d_V0^2.0
    double d_prov_c_1 = std::pow(d_vent,2.0); // d_vent^2.0

    c=((d_d * d_rho_air * d_SCx - 2.0 * d_masse) * d_prov_c_0 + 4.0 * d_d * d_rho_air * d_vent * d_SCx * d_V0 + 6.0 * d_d * d_rho_air * d_prov_c_1 * d_SCx + 4.0 * d_Er + 4.0 * d_Ep) / (8.0 * d_d);

    double d_prov_d_0 = std::pow(d_V0,2.0); // d_V0^2.0
    double d_prov_d_1 = std::pow(d_vent,2.0); // d_vent^2.0
    double d_prov_d_2 = std::pow(d_vent,3.0); // d_vent^3.0
    double d_prov_d_3 = std::pow(d_V0,3.0); // d_V0^3.0

    d=((d_d * d_rho_air * d_SCx - 2.0 * d_masse) * d_prov_d_3 + 4.0 * d_d * d_rho_air * d_vent * d_SCx * d_prov_d_0 + (6.0 * d_d * d_rho_air * d_prov_d_1 * d_SCx + 4.0 * d_Er + 4.0 * d_Ep) * d_V0 + 4.0 * d_d * d_rho_air * d_prov_d_2 * d_SCx - 8.0 * d_d * d_P) / (8.0 * d_d);

    double d_prov_p_0 = std::pow(b,2.0); // b^2.0
    double d_prov_p_1 = std::pow(a,2.0); // a^2.0

    p=(3.0 * a * c - d_prov_p_0) / (3.0 * d_prov_p_1);

    double d_prov_q_0 = std::pow(b,3.0); // b^3.0
    double d_prov_q_1 = std::pow(a,3.0); // a^3.0
    double d_prov_q_2 = std::pow(a,2.0); // a^2.0

    q=(27.0 * d_prov_q_2 * d - 9.0 * a * b * c + 2.0 * d_prov_q_0) / (27.0 * d_prov_q_1);

    double d_prov_discri_0 = std::pow(p,3.0); // p^3.0
    double d_prov_discri_1 = std::pow(q,2.0); // q^2.0

    discri=27.0 * d_prov_discri_1 + 4.0 * d_prov_discri_0;

    if(discri>0)
    {
        double d_prov_d_V1_0 = std::pow(p,3.0); // p^3.0
        double d_prov_d_V1_1 = std::pow(q,2.0); // q^2.0

        d_V1= - (6.0 * std::pow(18.0,(1.0 / 3.0)) * cos(atan2(0.0,(std::sqrt(3.0) * std::sqrt(27.0 * d_prov_d_V1_1 + 4.0 * d_prov_d_V1_0) - 9.0 * q) / 18.0) / 3.0) * p * std::pow((std::sqrt(3.0) * std::sqrt(27.0 * d_prov_d_V1_1 + 4.0 * d_prov_d_V1_0) - 9.0 * q),(2.0 / 3.0)) - std::pow(18.0,(2.0 / 3.0)) * cos(atan2(0.0,(std::sqrt(3.0) * std::sqrt(27.0 * d_prov_d_V1_1 + 4.0 * d_prov_d_V1_0) - 9.0 * q) / 18.0) / 3.0) * std::pow((std::sqrt(3.0) * std::sqrt(27.0 * d_prov_d_V1_1 + 4.0 * d_prov_d_V1_0) - 9.0 * q),(4.0 / 3.0))) / (18.0 * abs(std::sqrt(3.0) * std::sqrt(27.0 * d_prov_d_V1_1 + 4.0 * d_prov_d_V1_0) - 9.0 * q)) - b / (3.0 * a);

    }
    else
    {
        double d_prov_d_V1_0 = std::pow(p,3.0); // p^3.0
        double d_prov_d_V1_1 = std::pow(p,2.0); // p^2.0
        double d_prov_d_V1_2 = std::pow(q,2.0); // q^2.0

        d_V1=(std::sqrt(3.0) * std::pow(( - d_prov_d_V1_0),(5.0 / 6.0)) * cos(atan2(std::sqrt( - 27.0 * d_prov_d_V1_2 - 4.0 * d_prov_d_V1_0) / (2.0 * std::pow(3.0,(3.0 / 2.0))), - q / 2.0) / 3.0) + std::sqrt(3.0) * d_prov_d_V1_1 * std::pow(( - d_prov_d_V1_0),(1.0 / 6.0)) * cos(atan2(std::sqrt( - 27.0 * d_prov_d_V1_2 - 4.0 * d_prov_d_V1_0) / (2.0 * std::pow(3.0,(3.0 / 2.0))), - q / 2.0) / 3.0)) / (3.0 * d_prov_d_V1_1) - b / (3.0 * a);

    }
    if(d_V1<0)
        d_V1=0;

    if(std::isnan(d_V1))
            d_V1=d_V0;

    double d_prov_d_Ea_0 = std::pow(d_V0,2.0); // d_V0^2.0
    double d_prov_d_Ea_1 = std::pow(d_V1,2.0); // d_V1^2.0

    d_Ea=(d_masse * d_prov_d_Ea_1 - d_masse * d_prov_d_Ea_0) / 2.0;

    double d_prov_d_Ec_0 = std::pow(d_V1,3.0); // d_V1^3.0
    double d_prov_d_Ec_1 = std::pow(d_V0,2.0); // d_V0^2.0
    double d_prov_d_Ec_2 = std::pow(d_vent,2.0); // d_vent^2.0
    double d_prov_d_Ec_3 = std::pow(d_vent,3.0); // d_vent^3.0
    double d_prov_d_Ec_4 = std::pow(d_V0,3.0); // d_V0^3.0
    double d_prov_d_Ec_5 = std::pow(d_V1,2.0); // d_V1^2.0

    d_Ec=(d_d * d_rho_air * d_SCx * d_prov_d_Ec_0 + (d_d * d_rho_air * d_SCx * d_V0 + 4.0 * d_d * d_rho_air * d_vent * d_SCx) * d_prov_d_Ec_5 + (d_d * d_rho_air * d_SCx * d_prov_d_Ec_1 + 4.0 * d_d * d_rho_air * d_vent * d_SCx * d_V0 + 6.0 * d_d * d_rho_air * d_prov_d_Ec_2 * d_SCx) * d_V1 + d_d * d_rho_air * d_SCx * d_prov_d_Ec_4 + 4.0 * d_d * d_rho_air * d_vent * d_SCx * d_prov_d_Ec_1 + 6.0 * d_d * d_rho_air * d_prov_d_Ec_2 * d_SCx * d_V0 + 4.0 * d_d * d_rho_air * d_prov_d_Ec_3 * d_SCx) / (4.0 * d_V1 + 4.0 * d_V0);

}
