#include "route.h"

void route::go()
{
    queue S_queue;

    std::map<unsigned long long,node_parcouru> M_map;

    // on aura besoin du node d'arrive
    node n_arrivee = pD_db->get_node(ull_arrivee);

    // on commence par le noeud initial
    
    node_parcouru & P_depart = M_map[ull_depart];

    P_depart.set(ull_depart,pD_db,n_arrivee,m_model);

    P_depart.e_etat_cycliste_optimal = e_init;
    P_depart.d_cout_minimal = m_model.cout_etat(P_depart.e_etat_cycliste_optimal);
    P_depart.ull_from=0;

    double d_dist_min_total = P_depart.d_dist_min_arrive;
    double d_status=0;

    S_queue.push(node_in_queue(ull_depart,P_depart.d_cout_minimal + P_depart.d_cout_minimal_arrive));

    // Dans la queue on a que le noeud de départ et dans la map aussi.
    // On peut y aller tant que la queue n'est pas vide. Si elle le devient
    // c'est mauvais signe

    while( ! S_queue.empty() )
    {
        // on recupere le premier de la queue et on le gele

        unsigned long long ull_en_cours = S_queue.pop();
        node_parcouru & P_en_cours = M_map[ull_en_cours];
        P_en_cours.b_frozen=true;

        double d_nstatus=1-P_en_cours.d_dist_min_arrive/d_dist_min_total;
        if(d_nstatus>d_status)
        {
            d_status=d_nstatus;
            double d_realstatus = .5*(d_status*d_status+d_status)*d_status_echelle+d_status_offset;
            pD_db->write_status(d_realstatus);
        }

        // Si on vient de geler le noeud de fin, on s'arrete la.
        if(ull_en_cours == ull_arrivee)
        {
            break;
        }


        // on boucle sur les noeuds du voisinage
        std::list<unsigned long long> & lull_voisins = P_en_cours.lull_adj;

        for(std::list<unsigned long long>::iterator Ilull_voisin = lull_voisins.begin(); Ilull_voisin != lull_voisins.end();Ilull_voisin++)
        {
            unsigned long long ull_voisin= *Ilull_voisin;

            if(ull_voisin == P_en_cours.ull_from)
            {
                // C'est d'où on vient, on en va quand meme pas y retourner
                continue;
            }

            node_parcouru & P_voisin = M_map[ull_voisin];
            if(!P_voisin.defined())
            {
                // C'est la premiere fois qu'on touche ce noeud
                P_voisin.set(ull_voisin,pD_db,n_arrivee,m_model);
                P_voisin.d_cout_minimal = INFINITY;
            }

            if(P_voisin.b_frozen)
            {
                // Le voisin est gelé, on a déja un chemin optimal pour aller
                // jusqu'à lui
                continue;
            }

            // on calcul l'evolution de l'etat
            etat_cycliste etat_cycliste_voisin = m_model.energie_deplacement(P_en_cours.n_node,P_voisin.n_node,P_en_cours.e_etat_cycliste_optimal);

            if(m_model.cout_etat(etat_cycliste_voisin) < P_voisin.d_cout_minimal)
            {
                // Ce nouveau chemin est preferable pour aller a ce noeud
                P_voisin.d_cout_minimal=m_model.cout_etat(etat_cycliste_voisin);
                P_voisin.e_etat_cycliste_optimal=etat_cycliste_voisin;
                P_voisin.ull_from=ull_en_cours;
                S_queue.push(node_in_queue(ull_voisin,P_voisin.d_cout_minimal + P_voisin.d_cout_minimal_arrive));
            }
        }
    }

    node_parcouru & P_arrivee = M_map[ull_arrivee];

    if(!P_arrivee.b_frozen)
    {
        pD_db->write_state(19);
        abort();
    }

    std::list<unsigned long long> lull_chemin;
    {
        unsigned long long ull_courant=ull_arrivee;
        while(ull_courant!=0)
        {
            lull_chemin.push_front(ull_courant);
            ull_courant=M_map[ull_courant].ull_from;
        }
    }

    unsigned long long ull_previous_node;
    for(std::list<unsigned long long>::iterator it=lull_chemin.begin();it!=lull_chemin.end();it++)
    {
        instant I;

        node_parcouru & P_node = M_map[*it];
        node & n_node = P_node.n_node;
        etat_cycliste & e_node = P_node.e_etat_cycliste_optimal;
        

        I.ull_id=*it;
        I.d_lon=n_node.d_lon;
        I.d_lat=n_node.d_lat;
        I.d_height=n_node.d_height;
        I.d_dist=e_node.d_dist;
        I.d_V=e_node.d_V;
        I.d_ET=e_node.d_ET;
        I.d_deniv_pos=e_node.d_pente;

        if(it!=lull_chemin.begin())
        {
            std::pair<unsigned long long,unsigned int> pair = pD_db->get_edge(ull_previous_node,*it);
            I.ull_way_from_id=pair.first;
            I.ull_way_from_rev=pair.second;
        }
        ull_previous_node=*it;

        lI_result.push_back(I);
    }



    node_parcouru & P_last_node = M_map[ull_arrivee];

    d_dist_total = m_model.cout_etat_dist(P_last_node.e_etat_cycliste_optimal);
    d_deniv = m_model.cout_etat_pente(P_last_node.e_etat_cycliste_optimal);
    d_energie = m_model.cout_etat_energie(P_last_node.e_etat_cycliste_optimal);
    e_final = P_last_node.e_etat_cycliste_optimal;

    
}
            







