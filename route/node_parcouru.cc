#include "node_parcouru.h"

void node_parcouru::set(unsigned long long ull_id, database*  pD_db, const node & n_arrivee, const  model & m_model)
{
    n_node = pD_db->get_node(ull_id);
    lull_adj = pD_db->get_adjoining(ull_id);

    d_cout_minimal_arrive = m_model.cout_minimal(n_node,n_arrivee);
    d_dist_min_arrive = m_model.dist_node(n_node,n_arrivee);

    b_frozen = false;

    b_defined=true;
}

