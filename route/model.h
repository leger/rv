#ifndef H_ROUTAGE_MODEL
#define H_ROUTAGE_MODEL 1

#include "node.h"
#define G 9.81
#define RT 6371000
#include<math.h>
#include<stdio.h>
#include <stdlib.h>


class etat_cycliste
{
    public:

    double d_ET;
    double d_V;
    double d_dist;
    double d_pente;

    etat_cycliste() : d_ET(0),d_V(0),d_dist(0),d_pente(0) {}
    etat_cycliste(const double & d_ET_i, const double & d_V_i, const double & d_dist_i, const double & d_pente_i) : d_ET(d_ET_i), d_V(d_V_i), d_dist(d_dist_i), d_pente(d_pente_i) {}
    etat_cycliste(const etat_cycliste & orig) :
        d_ET(orig.d_ET),
        d_V(orig.d_V),
        d_dist(orig.d_dist),
        d_pente(orig.d_pente)
    {}

};

class model
{
    public:

    // type de model
    bool b_model_energie;
    bool b_model_dist;
    bool b_model_pente;

    double d_P; // W
    double d_Cr; // (sans unité)
    double d_SCx; // m^2
    double d_rho_air; // kg.m^(-3)
    double d_masse;   // kg
    double d_vent_from; // radian
    double d_vent_V; // m.s^(-1)

    model(char c_model_type, double d_VplatKMH=20, double d_masse_i=120, double d_Cr_i=.01, double d_SCx_i=.43, double d_rho_air_i=1.204, double d_vent_from_deg=0, double d_vent_VKMH=0);

    double dist_node(const node & n_node1, const node & n_node2) const;
    double proj_vent(const node & n_node1, const node & n_node2) const;

    
    etat_cycliste energie_deplacement(const node & n_node1, const node & n_node2, const etat_cycliste & e_etat_initial) const;
    void calc_model(double & d_V1, double & d_Ea, double & d_Ec, double d_V0, double d_vent, double d_d, double d_Ep, double d_Er) const;

    // cout minimal suivant le model
    double cout_minimal(const node & n_node_depart, const node & n_node_arrive) const;

    double cout_etat_dist(const etat_cycliste e_etat) const {return e_etat.d_dist;}
    double cout_etat_pente(const etat_cycliste e_etat) const {return e_etat.d_pente;};
    double cout_etat_energie(const etat_cycliste e_etat) const {return e_etat.d_ET - .5*d_masse*e_etat.d_V*e_etat.d_V;}

    double cout_etat(const etat_cycliste e_etat) const
    {
            if(b_model_energie)
                return cout_etat_energie(e_etat);
            if(b_model_dist)
                return cout_etat_dist(e_etat);
            return cout_etat_pente(e_etat);
    }                




};

#endif
