#ifndef H_NODE_PARCOURU
#define H_NODE_PARCOURU 1

#include "database.h"
#include "model.h"
#include<list>

class node_parcouru
{
    public:
    node n_node;
    std::list<unsigned long long> lull_adj;

    bool b_frozen;
    bool b_defined;

    etat_cycliste e_etat_cycliste_optimal;

    unsigned long long ull_from;
    double d_cout_minimal;

    double d_cout_minimal_arrive;
    double d_dist_min_arrive;

    node_parcouru() : b_defined(false),b_frozen(false),n_node(0,0,0,0) {}
        
    bool defined() {return b_defined;}

    void set(unsigned long long ull_id, database*  pD_db, const node & n_arrivee, const  model & m_model);
};




#endif
