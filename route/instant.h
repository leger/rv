#ifndef H_INSTANT
#define H_INSTANT 1

class instant
{
    public:
        unsigned long long ull_id;
        unsigned long long ull_way_from_id;
        unsigned int ull_way_from_rev;
        double d_lon;
        double d_lat;
        double d_height;
        double d_dist;
        double d_V;
        double d_ET;
        double d_deniv_pos;

    instant():
        ull_id(0),
        ull_way_from_id(0),
        ull_way_from_rev(0),
        d_height(0),
        d_dist(0),
        d_V(0),
        d_ET(0),
        d_deniv_pos(0)
    {}
};

#endif
