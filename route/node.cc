#include "node.h"

node::node(const unsigned long long & ull_id_i, const double & d_lon_i, const double & d_lat_i, const double & d_height_i)
{
    ull_id = ull_id_i;
    d_lon = d_lon_i;
    d_lat = d_lat_i;
    d_height = d_height_i;
}

node & node::operator=(const node & n_original)
{
    ull_id = n_original.ull_id;
    d_lon = n_original.d_lon;
    d_lat = n_original.d_lat;
    d_height = n_original.d_height;

    return(*this);
}
