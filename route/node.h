#ifndef H_ROUTAGE_NODE
#define H_ROUTAGE_NODE 1
class node
{
    public:

    unsigned long long ull_id;
    double d_lon;
    double d_lat;
    double d_height;

    node(const unsigned long long & ull_id_i, const double & d_lon_i, const double & d_lat_i, const double & d_height_i);

    node & operator=(const node & n_original);

    node(const node & n_orig) :
        ull_id(n_orig.ull_id),
        d_lon(n_orig.d_lon),
        d_lat(n_orig.d_lat),
        d_height(n_orig.d_height)
    {}
    

};

#endif
