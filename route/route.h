#ifndef H_ROUTE
#define H_ROUTE 1

#include "node_parcouru.h"
#include "model.h"
#include "node.h"
#include "queue.h"
#include "database.h"
#include<map>
#include "instant.h"

class route
{

    unsigned long long ull_depart;
    unsigned long long ull_arrivee;

    model m_model;

    database* pD_db;

    std::list<instant> lI_result;
    double d_dist_total;
    double d_deniv;
    double d_energie;

    etat_cycliste e_init;
    etat_cycliste e_final;

    double d_status_echelle;
    double d_status_offset;

    public:
    route(database* pD_db_i, const model & m_model_i, const unsigned long long & ull_id_1, const unsigned long long & ull_id_2) : 
        m_model(m_model_i),
        pD_db(pD_db_i) ,
        d_dist_total(0),
        d_deniv(0),
        d_energie(0),
        ull_depart(ull_id_1),
        ull_arrivee(ull_id_2),
        d_status_echelle(1),
        d_status_offset(0)
    {}

    std::list<instant> & get_result() { return lI_result;}
    double get_dist_total() { return d_dist_total; }
    double get_deniv() { return d_deniv; }
    double get_energie() { return d_energie; }
    etat_cycliste get_etat_final() { return e_final; }
    void set_etat_init(const etat_cycliste & e_init_i) {e_init=e_init_i;}
    void set_status(const double & d_status_echelle_i, const double & d_status_offset_i) {
        d_status_echelle=d_status_echelle_i;
        d_status_offset=d_status_offset_i;
    }

    void go();


};

#endif
