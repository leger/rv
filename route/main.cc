#include "route.h"
#include "model.h"
#include "database.h"

int main(int argc, char* argv[])
{

    int c;
    
    if(argc<3)
    {
        fprintf(stderr,"Usage incorrect\n");
        abort();
    }
    
    std::string s_db = argv[1];
    unsigned int jid = atoi(argv[2]);
    
    database D_db(s_db,jid);
    D_db.write_pid();
    
    
    if(D_db.get_state()!=0)
    {
        return(1);
    }
    
    D_db.write_state(1); // searching

    model m_model = D_db.get_model();
   


    double d_total_dist_min=0;
    std::list<unsigned long long> l_id;
    std::list<double> l_dists_min;


    if(!D_db.find_nodes(l_id))
    {
        D_db.write_state(17);
        return(1);
    }

    
    unsigned long long previous=0;

    // calcul des distances pour le status
    for(std::list<unsigned long long>::iterator it_id=l_id.begin();it_id!=l_id.end();it_id++)
    {
        if(it_id==l_id.begin())
        {
            previous=*it_id;
            continue;
        }

        double d_c=m_model.dist_node(D_db.get_node(*it_id),D_db.get_node(previous));
        l_dists_min.push_back(d_c);
        d_total_dist_min+=d_c;

        previous=*it_id;

    }

    if(d_total_dist_min>100e3)
    {
        D_db.write_state(18);
        return(1);
    }

    D_db.write_state(2); // routing

    previous=0;
    etat_cycliste e_courant;
    int troncon=0;
    double d_status_offset=0;

    std::list<instant> lI_globalres;

    std::list<double>::iterator it_dist_min=l_dists_min.begin();
    for(std::list<unsigned long long>::iterator it_id=l_id.begin();it_id!=l_id.end();it_id++)
    {
        if(it_id==l_id.begin())
        {
            previous=*it_id;
            continue;
        }

        double d_status_echelle=(*it_dist_min++)/d_total_dist_min;

        route r_route(&D_db,m_model,previous,*it_id);

        r_route.set_status(d_status_echelle,d_status_offset);
        r_route.set_etat_init(e_courant);
        
        r_route.go();
        
        e_courant=r_route.get_etat_final();
        d_status_offset+=d_status_echelle;


        std::list<instant> & lI_result = r_route.get_result();
        
        for(std::list<instant>::iterator it=lI_result.begin();it!=lI_result.end();it++)
        {
            lI_globalres.push_back(*it);
        }


        previous=*it_id;
        troncon++;

    }

    D_db.write_results(lI_globalres,m_model.d_P);
    D_db.write_state(3); // finito

    
    return(0);
}

