#include "model.h"

#include "calc_model.cc" // fichier generé à partir de gen_calc_model.pl
                         // (maxima requis)

#define AFFVAR(var) fprintf(stderr,#var"\t%f\n",var);
model::model(char c_model_type, double d_VplatKMH, double d_masse_i, double d_Cr_i, double d_SCx_i, double d_rho_air_i, double d_vent_from_deg, double d_vent_VKMH)
{

    b_model_energie=false;
    b_model_dist=false;
    b_model_pente=false;

    if(c_model_type=='e')
    {
        // energie
        b_model_energie=true;
    }
    else
    {
        if(c_model_type=='d')
        {
            b_model_dist=true;
        }
        else
        {
            if(c_model_type=='p')
            {
                b_model_pente=true;
            }
            else
            {
                fprintf(stdout,"# fatal error: model type\n");
                fflush(stdout);
                abort();
            }
        }
    }


    d_masse = d_masse_i;
    d_Cr = d_Cr_i;
    d_SCx = d_SCx_i;
    d_rho_air = d_rho_air_i;
    double d_Vplat = d_VplatKMH/3.6;
    d_P=(d_masse*G*d_Cr+.5*d_rho_air*d_SCx*d_Vplat*d_Vplat)*d_Vplat;

    d_vent_from = M_PI*.5 - d_vent_from_deg/180*M_PI;
    d_vent_V = d_vent_VKMH/3.6;

}

double model::dist_node(const node & n_node1, const node & n_node2) const
{
    double d_a=cos((n_node1.d_lat+n_node2.d_lat)/2/180*M_PI)*(n_node1.d_lon-n_node2.d_lon)/180*M_PI*RT;
    double d_b=(n_node1.d_lat-n_node2.d_lat)/180*M_PI*RT;

    double d_d = sqrt(d_a*d_a + d_b*d_b);

    return(d_d);
}

double model::proj_vent(const node & n_node1, const node & n_node2) const
{
    double d_a=cos((n_node1.d_lat+n_node2.d_lat)/2/180*M_PI)*(n_node2.d_lon-n_node1.d_lon)/180*M_PI*RT;
    double d_b=(n_node2.d_lat-n_node1.d_lat)/180*M_PI*RT;

    double d_d = sqrt(d_a*d_a + d_b*d_b);

    double d_scal;
    if(d_d>0)
        d_scal = (cos(d_vent_from)*d_a + sin(d_vent_from)*d_b)/d_d;
    else
        d_scal=0;

    return(d_scal*d_vent_V);
}

etat_cycliste model::energie_deplacement(const node & n_node1, const node & n_node2, const etat_cycliste & e_etat_initial) const
{
    const double & d_V0 = e_etat_initial.d_V;
    const double & d_h0 = n_node1.d_height;
    const double & d_hf = n_node2.d_height;
    
    double d_d = dist_node(n_node1, n_node2);
    double d_vent = proj_vent(n_node1, n_node2);


    double d_Ep = d_masse * G * (d_hf-d_h0);
    double d_Er = d_masse * G * d_Cr * d_d;

    double d_Vf;
    double d_Ec;
    double d_Ea;
    calc_model(d_Vf, d_Ea, d_Ec, d_V0, d_vent, d_d, d_Ep, d_Er);

    double d_ET = d_Ea + d_Ec + d_Ep + d_Er;



    double d_pente = (d_hf-d_h0>0) ? d_hf-d_h0 : 0;

    etat_cycliste e_etat_final(e_etat_initial.d_ET + d_ET, d_Vf, e_etat_initial.d_dist + d_d, e_etat_initial.d_pente + d_pente);

    return e_etat_final;
}
        
double model::cout_minimal(const node & n_node, const node & n_node_arrivee) const
{   
    //etat_cycliste e_defaut(0,d_Vasserv,0,0);

    double d_cout;

    if(b_model_energie)
    {
        double d_d = dist_node(n_node, n_node_arrivee);
        const double & d_h0 = n_node.d_height;
        const double & d_hf = n_node_arrivee.d_height;

        double d_Ep = d_masse*G*(d_hf-d_h0);
        double d_Er = d_masse*G*d_Cr*d_d;

        d_cout = d_Ep + d_Er;
    }

    if(b_model_dist)
    {
        d_cout = dist_node(n_node, n_node_arrivee);
    }

    if(b_model_pente)
    {
        const double & d_h0 = n_node.d_height;
        const double & d_hf = n_node_arrivee.d_height;
        d_cout = d_hf-d_h0;
    }

    if(d_cout<0)
        d_cout=0;

    return(d_cout);
}
