
-- 
-- modifs
DROP FUNCTION IF EXISTS updateRV();
CREATE FUNCTION updateRV() RETURNS VOID AS $$
DECLARE
    update_id integer;
BEGIN

    SELECT rv_timing_init() INTO update_id;

    -- update de rv_way
    -- etape 1 : effacement des ways effacée ou modifiée
    DELETE FROM rv_ways
        USING
            (
                SELECT 
                        id
                    FROM
                        rv_to_update
                    WHERE
                            data_type='W'
             ) AS sub
        WHERE 
            rv_ways.id=sub.id;

    PERFORM rv_timing(update_id); -- 1
    -- etape 2 : on crée les ways nouvelles et les modifiés (qui viennet d'être
    -- effacées
    INSERT INTO rv_ways 
        (
            SELECT
                    ways.id AS id
                FROM 
                        ways 
                    INNER JOIN 
                        rv_to_update ON ways.id=rv_to_update.id
                WHERE 
                        data_type='W' 
                    AND 
                        (action='C' OR action='M')
                    AND
                        (
                                tags->'highway' NOT SIMILAR TO '(motorway|trunk|construction|steps|pedestrian|track)%' 
                            OR
                                tags->'cycleway' NOT SIMILAR TO '(no|false)'
                            OR
                                tags->'bicycle' NOT SIMILAR TO '(no|false)'
                        )
        );

    PERFORM rv_timing(update_id); -- 2
    -- rv_edges
    -- etape 1 : on effacte tous les edges qui proviennent d'un ways
    DELETE FROM rv_edges
        USING
            (
                SELECT 
                        id
                    FROM
                        rv_to_update
                    WHERE
                            data_type='W' 
            ) AS sub
        WHERE
            way_id=sub.id;

    PERFORM rv_timing(update_id); -- 3
    
    -- etape 2 : on crée tous les edges des ways crées ou modifés
    PERFORM compute_edges_for_one_way(sub.id)
        FROM
            (
                SELECT
                        rv_ways.id
                    FROM 
                            rv_ways
                        INNER JOIN 
                            rv_to_update ON rv_ways.id=rv_to_update.id
                    WHERE 
                            data_type='W'
                        AND
                            (action='C' OR action='M')
            ) AS sub;

    PERFORM rv_timing(update_id); -- 4
    
    -- rv_nodes
    -- etape 1: on considère que tous les nodes apparenants à des ways modifiés
    -- ont été potentiellement modifés (la ways peut avoir été marqué comme
    -- interdite au cyclistes)
    INSERT INTO rv_to_update
        (
            SELECT
                    'N' AS data_type,
                    'M' AS action,
                    n.id AS id
                FROM
                    (
                            (
                                SELECT DISTINCT
                                        node_id AS id
                                    FROM 
                                            rv_to_update 
                                        INNER JOIN 
                                            way_nodes ON way_nodes.way_id=rv_to_update.id
                                    WHERE
                                        data_type='W'
                            )
                        EXCEPT
                            (
                                SELECT
                                        id
                                    FROM 
                                        rv_to_update
                                    WHERE
                                        data_type='N'
                            )
                    ) AS n
        );


    PERFORM rv_timing(update_id); -- 5
    
    -- etape 2 : on effacte tous les nœuds supprimés ou modifés
    DELETE FROM rv_nodes 
        USING 
            (
                SELECT
                        id AS node_id
                    FROM
                        rv_to_update
                    WHERE
                        data_type='N'
            ) AS sub 
        WHERE 
            rv_nodes.id=sub.node_id;

    
    PERFORM rv_timing(update_id); -- 6
    
    -- etape 3 : on insere (ou re-insere) tous les noeuds crées (ou modifiés)
    INSERT INTO rv_nodes
        (
            SELECT
                    n.id AS id,
                    get_height(ST_X(nodes.geom),ST_Y(nodes.geom)) AS height,
                    NULL::bigint AS cc,
                    nodes.geom AS geom
                FROM
                        (
                            SELECT DISTINCT
                                    rv_to_update.id AS id
                                FROM
                                        rv_to_update
                                    INNER JOIN 
                                        way_nodes ON way_nodes.node_id=rv_to_update.id
                                    INNER JOIN 
                                        rv_ways ON way_nodes.way_id=rv_ways.id
                                WHERE 
                                        data_type='N'
                                    AND 
                                        (action='C' OR action='M') 
                        ) AS n
                    INNER JOIN
                        nodes ON nodes.id=n.id
        );

    -- computation of connected components
    -- puis on mets à jour les labels de cc à partir de ceux calculés sur
    -- le graph dual

    PERFORM rv_timing(update_id); -- 7
    
    CREATE TEMP TABLE rv_way_way (id1 BIGINT, id2 BIGINT);
    INSERT INTO rv_way_way
        (
            SELECT DISTINCT
                    w1.id AS id1,
                    w2.id AS id2
                FROM
                        rv_ways AS w1
                    INNER JOIN
                        way_nodes AS wn1 ON wn1.way_id=w1.id
                    INNER JOIN
                        way_nodes AS wn2 ON wn2.node_id=wn1.node_id AND wn2.way_id<>wn1.way_id
                    INNER JOIN
                        rv_ways AS w2 ON w2.id=wn2.way_id
         );
    INSERT INTO rv_way_way
        (
            SELECT
                    w.id AS id1,
                    w.id AS id2
                FROM
                        rv_ways AS w
         );

    PERFORM rv_timing(update_id); -- 8
    
    CREATE TEMP TABLE rv_way_cc (way_id BIGINT, cc_id BIGINT);
    INSERT INTO rv_way_cc
        (
            SELECT
                    w.id AS way_id,
                    w.id AS cc_id
                FROM
                        rv_ways AS w
         );

    PERFORM rv_timing(update_id); -- 9
    
    PERFORM propto();

    PERFORM rv_timing(update_id); -- 10
    
    UPDATE rv_nodes 
        SET 
            cc=sub.cc
        FROM
            (
                SELECT DISTINCT 
                        wn.node_id AS id,
                        wc.cc_id AS cc
                    FROM 
                            rv_way_cc AS wc
                        INNER JOIN 
                            way_nodes AS wn ON wn.way_id=wc.way_id 
                        INNER JOIN
                            rv_nodes ON rv_nodes.id=wn.node_id 
            ) AS sub 
            WHERE
                sub.id=rv_nodes.id;


    PERFORM rv_timing(update_id); -- 11

END;
$$ LANGUAGE plpgsql;
