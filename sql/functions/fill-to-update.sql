
-- 
-- modifs
DROP FUNCTION IF EXISTS rv_fill_to_update();
CREATE FUNCTION rv_fill_to_update() RETURNS VOID AS $$
BEGIN

    DELETE 
        FROM 
            rv_to_update
        USING
            actions
        WHERE
                actions.data_type = rv_to_update.data_type
            AND
                actions.id = rv_to_update.id;

    INSERT INTO rv_to_update (SELECT * FROM actions);
        
END;
$$ LANGUAGE plpgsql;
