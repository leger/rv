DROP FUNCTION IF EXISTS propto();
CREATE FUNCTION propto() RETURNS VOID AS $$
DECLARE
    nb BIGINT;
    k BIGINT;
BEGIN
    nb:=1;

    WHILE nb<>0 LOOP


WITH
    mm AS
        (
            SELECT
                    wc1.cc_id AS old_cc_id,
                    min(wc2.cc_id) AS new_cc_id
                FROM
                        rv_way_cc AS wc1
                    INNER JOIN
                        rv_way_way AS ww ON wc1.way_id=ww.id1
                    INNER JOIN
                        rv_way_cc AS wc2 ON ww.id2=wc2.way_id
                GROUP BY
                    wc1.cc_id
        )
    UPDATE rv_way_cc
        SET
            cc_id=sub.new_cc_id
        FROM
            (
                SELECT * FROM mm WHERE NOT old_cc_id=new_cc_id
            ) AS SUB
        WHERE
            cc_id=sub.old_cc_id;

        GET DIAGNOSTICS nb = ROW_COUNT;
        RAISE NOTICE '% ways updated', nb;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

