DROP FUNCTION IF EXISTS rv_timing_init();
CREATE FUNCTION rv_timing_init() RETURNS integer AS $$
BEGIN
    RETURN 0::integer;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS rv_timing(integer);
CREATE FUNCTION rv_timing(id integer) RETURNS VOID AS $$
BEGIN
END;
$$ LANGUAGE plpgsql;
