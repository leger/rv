DROP FUNCTION IF EXISTS rv_timing_init();
CREATE FUNCTION rv_timing_init() RETURNS integer AS $$
DECLARE
    id integer;
    n_w integer;
    n_n integer;
    t timestamp;
BEGIN
    SELECT count(*) FROM rv_to_update WHERE data_type='N' INTO n_n;
    SELECT count(*) FROM rv_to_update WHERE data_type='W' INTO n_w;

    SELECT timeofday()::TIMESTAMP INTO t;

    INSERT INTO rv_update_timing 
            (n_ways,n_nodes,update_begin,update_end)
        VALUES
            (n_w,n_n,t,t)
        RETURNING update_id INTO id;

    RETURN id;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS rv_timing(integer);
CREATE FUNCTION rv_timing(id integer) RETURNS VOID AS $$
DECLARE
    t1 timestamp;
    t2 timestamp;
    k integer;
    dt float;
BEGIN
    
    SELECT 
            *
        FROM 
            (
                (
                    SELECT 
                            1::integer 
                        FROM 
                            rv_update_timing
                        WHERE 
                                array_length(update_times,1) IS NULL
                            AND update_id=id
                ) 
                UNION
                (
                    SELECT
                            1+array_length(update_times,1)
                        FROM 
                            rv_update_timing 
                        WHERE 
                                NOT array_length(update_times,1) IS NULL 
                            AND 
                                update_id=id
                )
            ) AS sub 
        INTO k;

    SELECT update_end FROM rv_update_timing WHERE update_id=id INTO t1;
    SELECT timeofday()::TIMESTAMP INTO t2;
    SELECT date_part('epoch',(t2-t1)) INTO dt;

    UPDATE rv_update_timing SET update_end=t2 WHERE update_id=id;
    UPDATE rv_update_timing SET update_times[k]=dt WHERE update_id=id;

    RAISE NOTICE 'rv_timing: step %: % seconds', k, dt;
END;
$$ LANGUAGE plpgsql;
