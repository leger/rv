
-- 
-- modifs
DROP FUNCTION IF EXISTS osmosisUpdate();
CREATE FUNCTION osmosisUpdate() RETURNS VOID AS $$
BEGIN
    PERFORM rv_fill_to_update();
END;
$$ LANGUAGE plpgsql;
