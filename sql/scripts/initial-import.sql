
BEGIN;

    -- locks only for wrinting this is not blocking reading
    LOCK TABLE nodes IN EXCLUSIVE MODE;
    LOCK TABLE ways IN EXCLUSIVE MODE;
    LOCK TABLE way_nodes IN EXCLUSIVE MODE;
    LOCK TABLE actions IN EXCLUSIVE MODE;
    LOCK TABLE rv_nodes IN EXCLUSIVE MODE;
    LOCK TABLE rv_ways IN EXCLUSIVE MODE;
    LOCK TABLE rv_edges IN EXCLUSIVE MODE;

    -- this is blocking reading and wrinting
    LOCK TABLE rv_to_update IN ACCESS EXCLUSIVE MODE;

    
    INSERT INTO rv_ways 
        (
            SELECT
                    ways.id AS id
                FROM 
                        ways 
                WHERE 
                        (
                                tags->'highway' NOT SIMILAR TO '(motorway|trunk|construction|steps|pedestrian|track)%' 
                            OR
                                tags->'cycleway' NOT SIMILAR TO '(no|false)'
                            OR
                                tags->'bicycle' NOT SIMILAR TO '(no|false)'
                        )
        );

    SELECT sum(compute_edges_for_one_way(sub.id))
        FROM
            (
                SELECT
                        rv_ways.id
                    FROM 
                            rv_ways
            ) AS sub;


    -- etape 3 : on insere (ou re-insere) tous les noeuds crées (ou modifiés)
    INSERT INTO rv_nodes
        (
            SELECT
                    n.id AS id,
                    get_height(ST_X(nodes.geom),ST_Y(nodes.geom)) AS height,
                    NULL::bigint AS cc,
                    nodes.geom AS geom
                FROM
                        (
                            SELECT DISTINCT
                                    way_nodes.node_id AS id
                                FROM
                                        way_nodes
                                    INNER JOIN 
                                        rv_ways ON way_nodes.way_id=rv_ways.id
                        ) AS n
                    INNER JOIN
                        nodes ON nodes.id=n.id
        );

    -- for the computation of cc
    SELECT updateRV();

COMMIT;
