BEGIN;

    DROP TABLE IF EXISTS rv_update_timing;
    CREATE TABLE rv_update_timing 
        (
            update_id SERIAL CONSTRAINT pk_rv_update_timing PRIMARY KEY,
            n_ways INTEGER,
            n_nodes INTEGER,
            update_begin TIMESTAMP, 
            update_end TIMESTAMP, 
            update_times FLOAT[]
        );

COMMIT;
