BEGIN;
    -- rv_height
    DROP TABLE IF EXISTS rv_height;
    CREATE TABLE rv_height (lon_s INTEGER, lat_s INTEGER, height FLOAT, approx BOOLEAN);

    ALTER TABLE rv_height ADD CONSTRAINT pk_rv_height PRIMARY KEY (lon_s,lat_s);

    SELECT AddGeometryColumn('rv_height', 'geom', 4326, 'POINT', 2);
    CREATE INDEX idx_rv_height_geom ON rv_height USING gist (geom);
    CLUSTER rv_height USING idx_rv_height_geom;

    CREATE INDEX idx_rv_height_approx ON rv_height USING btree (approx);
COMMIT;
