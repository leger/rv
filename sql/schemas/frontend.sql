
BEGIN;
    -- rv_jobs
    DROP TABLE IF EXISTS rv_jobs;

    CREATE TABLE rv_jobs (jid SERIAL CONSTRAINT pk_rv_jobs PRIMARY KEY,
                          expire TIMESTAMP,
                          Cr FLOAT CHECK (Cr>0),
                          SCx FLOAT CHECK (SCx>0),
                          criterion CHAR(1),
                          mass FLOAT CHECK (mass>0),
                          rho FLOAT CHECK (rho>0),
                          speedref FLOAT CHECK (speedref>0),
                          winddir FLOAT,
                          windspeed FLOAT,
                          cum_elev FLOAT,
                          energy FLOAT,
                          dist FLOAT,
                          mean_speed FLOAT,
                          time FLOAT,
                          state INTEGER,
                          status FLOAT,
                          pid INTEGER DEFAULT 0
                         );

-- rv_results
    DROP TABLE IF EXISTS rv_results;
    CREATE TABLE rv_results (jid INTEGER,
                             rank INTEGER,
                             height FLOAT,
                             speed FLOAT,
                             dist FLOAT,
                             cum_elev FLOAT,
                             energy FLOAT,
                             time FLOAT,
                             node_id BIGINT,
                             way_id BIGINT
                            );


    SELECT AddGeometryColumn('rv_results', 'geom', 4326, 'POINT', 2);
    ALTER TABLE rv_results ADD CONSTRAINT pk_rv_results PRIMARY KEY (jid,rank);
    CREATE INDEX idx_rv_results_jid ON rv_results USING btree (jid);

    -- rv_waypoints
    DROP TABLE IF EXISTS rv_waypoints;
    CREATE TABLE rv_waypoints (jid INTEGER,
                               rank INTEGER);
    
    SELECT AddGeometryColumn('rv_waypoints', 'geom', 4326, 'POINT', 2);
    ALTER TABLE rv_waypoints ADD CONSTRAINT pk_rv_waypoints PRIMARY KEY (jid,rank);
    CREATE INDEX idx_rv_waypoints_jid ON rv_waypoints USING btree (jid);


COMMIT;


